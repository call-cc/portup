# portup
an interace to poudriere

# installation
run `chmod +x portup` and move it to your /usr/local/bin

# usage
run `sudo portup --init` to initialize portup. this requires a working poudriere configuration file

run `sudo portup -b <category>/<package>` to build a package, and after that is done run `sudo portup -i <package>` to install it

updating your ports is as easy as running `sudo portup -u`

# contribution
if my jail happens to be outdated please submit an issue, otherwise just send a pull request and i'll approve it
